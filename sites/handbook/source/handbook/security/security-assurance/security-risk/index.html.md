---
layout: handbook-page-toc
title: "Security Risk Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

<!--HTML Parser Markup-->
{::options parse_block_html="true" /}

## <i class="fas fa-bullseye" style="color:rgb(110,73,203)" aria-hidden="true"></i> Security Risk Mission
{: #security-risk-mission}

To improve security at GitLab by enabling informed and intelligent decision making through proactive identification, monitoring, and reporting of security risks.

## <i class="far fa-lightbulb" style="color:rgb(110,73,203)" aria-hidden="true"></i> Core Competencies
{: #core-competencies}
----

### <i class="fas fa-shield-alt" style="color:rgb(253,109,38)" aria-hidden="true"></i> Security Operational Risk Management (StORM) Program
{: #storm}

An integrated Operational Risk Management program which focuses on the identification, assessment, continuous monitoring, and reporting of security risks across the organization. Visit the [StORM Program & Procedures](/handbook/security/security-assurance/security-risk/storm-program/index.html) handbook page for additional details, including a quick introduction to Risk Management at GitLab as well as information about the purpose, scope, and specific procedures executed as part of the program. 


<div class="panel panel-gitlab-orange">
**Need to communicate a potential risk to the team?**
{: .panel-heading}
<div class="panel-body">

Please refer to the [communication section of the StORM Program & Procedures](/handbook/security/security-assurance/security-risk/storm-program/index.html#communication-of-risks-to-the-security-risk-team) page for information on the various ways that team members can use to escalate potential risks to the Security Risk Team.

</div>
</div>

### <i class="fas fa-hands-helping" style="color:rgb(253,109,38)" aria-hidden="true"></i> Security Third Party Risk Management (TPRM) Program
{: #tprm}

The TPRM Program is focused on identifying and assessing the incremental security risk impact that may develop over the lifecycle of GitLab's relationship with various third parties. Our program is integrated within the [Procurement](/handbook/finance/procurement/) process and is built to continuously monitor third parties based on risk. Additional information can be found on the [Third Party Risk Management](/handbook/security/security-assurance/security-risk/third-party-risk-management.html) handbook page.

### <i class="fas fa-exclamation-triangle" style="color:rgb(253,109,38)" aria-hidden="true"></i> Business Impact Analysis (BIA) and Critical System Tiering (CST)
{: #bia}

The [Business Impact Analysis](/handbook/security/security-assurance/security-risk/storm-program/business-impact-analysis.html) (BIA) helps determine the systems critical to serving GitLab's Customers. It also helps determine the prioritization of system restoration efforts in the event of a disruption.  

The Security Risk Team facilitates a BIA for all new systems. A BIA is performed or previously collected BIA data is validated for existing systems based on [Critical System Tiering (CST)](/handbook/security/security-assurance/security-risk/storm-program/critical-systems.html#critical-systems-tiering-methodology).

### <i class="fas fa-warehouse" style="color:rgb(253,109,38)" aria-hidden="true"></i> Asset Inventory Maintenance
{: #asset-inventory}

Establishing a complete and accurate inventory of assets is key to the success of GitLab's Risk Program. As such, the Security Risk Team collaborates closely with IT and Business Owners to ensure new systems are added to the [Tech Stack](/handbook/business-technology/tech-stack-applications/#roles-and-responsibilities) and that associated data is maintained via our BIA processes.

----
## <i class="fas fa-tasks" style="color:rgb(110,73,203)" aria-hidden="true"></i> Metrics and Measures of Success
{: #metrics}

- [StORM Program Risk Heatmap](/handbook/security/performance-indicators/#operational-security-risk-management-tier-2-risks)
- [Third Party Risk Management - Residual Risk Ratings](/handbook/security/performance-indicators/#third-party-risk-management)
<br>


## <i class="fas fa-users" style="color:rgb(110,73,203)" aria-hidden="true"></i> Team Members
{: #team-members}

|Team Member|Role|
|:----------:|:----------:|
|[Ty Dilbeck](https://gitlab.com/tdilbeck)|[Manager, Security Risk](handbook/job-families/security/security-risk/#manager-security-risk)|
|[Eric Geving](https://gitlab.com/ericgeving)|[Senior Security Risk Engineer](handbook/job-families/security/security-risk/#senior-security-risk-engineer)|
|[Kyle Smith](https://gitlab.com/kylesmith2)|[Senior Security Risk Engineer](handbook/job-families/security/security-risk/#senior-security-risk-engineer)|
|[Ryan Lawson](https://gitlab.com/rlawson1)|[Senior Security Risk Engineer](handbook/job-families/security/security-risk/#senior-security-risk-engineer)|
|[Nirmal Devarajan](https://gitlab.com/ndevarajan)|[Security Assurance Engineer](handbook/job-families/security/security-risk/#security-risk-engineer-intermediate)|

## <i class="fa-solid fa-d" style="color:rgb(110,73,203)" aria-hidden="true"></i>Functional DRIs
{: #dris}

While the [DRI](/handbook/people-group/directly-responsible-individuals/#characteristics-of-a-project-dri) is the individual who is ultimately held accountable for the success or failure of any given project, they are not necessarily the individual that does the tactical project work. The DRI should consult and collaborate with all teams and stakeholders involved to ensure they have all relevant context, to gather input/feedback from others, and to divide action items and tasks amongst those involved.

DRIs are responsible for ensuring a [handbook-first approach](/company/culture/all-remote/handbook-first-documentation/) to their project(s) and challenging existing processes for [efficiency](/handbook/values/#efficiency).

|Function	|DRI|
|:----------:|:----------:|
|[Annual Risk Assessment](/handbook/security/security-assurance/security-risk/storm-program/#storm-procedures)	|Kyle Smith|
|[Business Impact Analysis - Design And Requirements](/handbook/security/security-assurance/security-risk/storm-program/business-impact-analysis.html)	|Kyle Smith|
|[Business Impact Analysis - Reporting and Periodic BIA Execution](/handbook/security/security-assurance/security-risk/storm-program/business-impact-analysis.html)	|Nirmal Devarajan|
|[Critical System Tiering](/handbook/security/security-assurance/security-risk/storm-program/critical-systems.html#determining-critical-system-tiers)	|Kyle Smith|
|[Ongoing SecRisk-Related Observations Management](/handbook/security/security-assurance/observation-management-procedure.html#introduction-to-observation-management-at-gitlab)	|Ty Dilbeck|
|[Ongoing Risk Treatment](/handbook/security/security-assurance/security-risk/storm-program/#storm-procedures)	|Kyle Smith|
|[Ongoing TPRM Assessments](/handbook/security/security-assurance/security-risk/third-party-risk-management.html)	|Ryan Lawson|
|[Periodic SOX CUEC Facilitation](/handbook/security/security-assurance/security-risk/sox_cuec_mapping_procedure.html)	|Eric Geving|
|[Periodic TPRM Assessments](/handbook/security/security-assurance/security-risk/third-party-risk-management.html)	|Eric Geving|
|TPRM Data Quality and Emerging Requirements Management | Eric Geving |
|[StORM Metrics and Reporting](/handbook/security/security-assurance/security-risk/storm-program/#step-5-annual-storm-reports)	|Kyle Smith|
|TPRM Metrics and Reporting	|Ryan Lawson|

## <i class="fas fa-id-card" style="color:rgb(110,73,203)" aria-hidden="true"></i> Contact the Team
{: #contact}

- <i class="fas fa-envelope fa-fw" style="color:rgb(219,59,33)" aria-hidden="true"></i> Email: `securityrisk@gitlab.com`
- <i class="fab fa-slack fa-fw" style="color:rgb(219,59,33)" aria-hidden="true"></i> Slack: 
   - [#security-risk-management channel](https://gitlab.slack.com/archives/C01EKDNRVFD)
   - [#sec-assurance channel](https://gitlab.slack.com/archives/C0129P7DW75) (includes the broader Security Assurance Team)
   - Mention `@security-risk`
- <i class="fab fa-gitlab fa-fw" style="color:rgb(219,59,33)" aria-hidden="true"></i> GitLab: Tag the team across GitLab using `@gitlab-com/gl-security/security-assurance/security-risk-team`

<div class="flex-row" markdown="0" style="height:40px">
    <a href="https://about.gitlab.com/handbook/security/security-assurance/#" class="btn btn-purple-inv" style="width:100%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Return to the Security Assurance Homepage</a>
</div> 
